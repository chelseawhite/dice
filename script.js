//initialize count
const count = [0,0,0,0,0,0,0,0,0,0,0,0,0];

//Found random number function on MDN

//Rolls each individual die
function rollOne(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max-min + 1)) + min
}

function rollTwo(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max-min + 1)) + min
}

//Rolls both dice together
function pairRolled(rollOne,rollTwo) {
    for (let i=0; i<1000; i++) {
        let bothRolled = rollOne(1,6) + rollTwo(1,6);
        count[bothRolled] +=1
    }
    return count;
}

pairRolled(rollOne,rollTwo)
console.log(count)

//Prints results to HTML
for(let i=2; i<count.length; i++){
    let counterDiv = document.createElement('div')
    counterDiv.innerHTML = i+": "+count[i];
    document.getElementById('counts').appendChild(counterDiv)
}

//Adds bar height
for(let i=2; i<count.length; i++){
   let newDiv = document.createElement('div')
   newDiv.style.height = count[i] + 'px',
   newDiv.style.width = '30px',
   newDiv.style.backgroundColor = '#2267AB',
   newDiv.style.display = 'inline-block',
   newDiv.style.marginRight = '5px',
   document.getElementById('bars').appendChild(newDiv)
}




